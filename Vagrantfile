# -*- mode: ruby -*-
# vi: set ft=ruby :

required_plugins = %w( vagrant-guest_ansible vagrant-hostmanager nugrant )

plugins_to_install = required_plugins.select { |plugin| not Vagrant.has_plugin? plugin }
if not plugins_to_install.empty?
    puts "Installing plugins: #{plugins_to_install.join(' ')}"
    if system "vagrant plugin install #{plugins_to_install.join(' ')}"
        puts "Successfully installed plugins, run `vagrant #{ARGV.join(' ')}` again"
        exit
    else
        abort "Installation of one or more plugins has failed. Aborting."
    end
end

Vagrant.configure(2) do |config|
    config.vm.box = "ubuntu/trusty64"
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.manage_guest = true
    config.hostmanager.ignore_private_ip = false
    config.user.defaults = {
        "env" => "develop"
    }

    config.vm.define "dev" do |dev|
        dev.vm.network "private_network", ip: "192.168.2.16"
        dev.vm.hostname = 'sandbox-dev'
        dev.hostmanager.aliases = %w(sandbox.dev)

        if config.user.env != "test"
            dev.vm.synced_folder ".", "/vagrant", :nfs => { :mount_options => ["dmode=777","fmode=666"] }

            dev.vm.provider "virtualbox" do |v|
                v.memory = 3072
                v.cpus = 2
            end
        else
            dev.vm.synced_folder ".", "/vagrant",
                :rsync__exclude => ["webapp/node_modules", "backend/vendor"],
                :rsync__auto => false,
                :type => "rsync",
                :mount_options => ["dmode=775","fmode=664"]
        end

        dev.vm.provision :hostmanager

        dev.vm.provision "shell", path: "provisioning/setup.sh"

        dev.vm.provision :guest_ansible do |guest_ansible|
            guest_ansible.playbook = "provisioning/vagrant.yml"
            guest_ansible.inventory_path = "vagrant_inventory"
            guest_ansible.extra_vars = {:remote_tmp => "/vagrant/ansible/tmp", :log_path => "/vagrant/ansible/ansible.log"}
        end

        if config.user.env == "test"
            dev.vm.provision :guest_ansible do |guest_ansible|
                guest_ansible.playbook = "provisioning/test_vagrant.yml"
                guest_ansible.inventory_path = "vagrant_inventory"
                guest_ansible.extra_vars = {:remote_tmp => "/vagrant/ansible/tmp", :log_path => "/vagrant/ansible/ansible.log"}
            end
        end
    end
end
