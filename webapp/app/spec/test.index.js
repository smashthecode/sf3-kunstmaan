window.ngInject = (fn) => fn;
window.jQuery = window.$ = require('jquery');

require('angular');
require('angular-mocks/angular-mocks');

var appContext = require.context('../src', true, /\.js$/);
appContext.keys().forEach(appContext);
