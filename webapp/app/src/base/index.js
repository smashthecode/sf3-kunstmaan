import angular from 'angular';
import translate from 'angular-translate';
import ngMaterial from 'angular-material';

export default angular.module('base', [
  translate,
  ngMaterial
]).name;
