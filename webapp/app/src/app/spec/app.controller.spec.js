import angular from 'angular';

import appModule from '../index';

describe('Controller: App', () => {
  let $scope, $state, $controller, $window;

  beforeEach(angular.mock.module(appModule));

  beforeEach(angular.mock.inject(($rootScope, _$controller_, _$state_, _$window_) => {
    $controller = _$controller_;
    $state = _$state_;
    $window = _$window_;
    $scope = $rootScope.$new();

    $controller('AppController', {
      $scope,
      locale: {lang: 'some-lang'}
    });

    spyOn($state, 'go');
  }));


  it('should scroll document to top', () => {
    spyOn($window, 'scrollTo');

    $scope.$emit('$viewContentLoaded');

    expect($window.scrollTo).toHaveBeenCalledWith(0, 0);
  });
});
