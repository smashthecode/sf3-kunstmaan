import angular from 'angular';
import uiRouter from 'angular-ui-router';

import routing from './error404.routes';

export default angular.module('app.error', [
  uiRouter
]).config(routing)
  .name;
