
import appTemplate from './app.html';

export default ngInject(($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider,
                         $locationProvider) => {
  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/404');
  $urlMatcherFactoryProvider.strictMode(false);

  $stateProvider
    .state('app', {
      'abstract': true,
      url: '/',
      template: appTemplate,
      controller: 'AppController',
      controllerAs: 'app'
    });
});
