export default ngInject(function AppController($rootScope, $state, $window) {
  $rootScope.$on('$viewContentLoaded', () => {
    $window.scrollTo(0, 0);
  });
});
