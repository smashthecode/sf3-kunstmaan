import angular from 'angular';
import ngSanitize from 'angular-sanitize';
import ngCookies from 'angular-cookies';
import ngAria from 'angular-aria';
import ngMessages from 'angular-messages';
import ngMaterial from 'angular-material';
import uiRouter from 'angular-ui-router';
import angularElastic from 'angular-elastic';
import 'angular-froala';

/**
 * Modules
 */
import config from '../config';
import base from '../base';
import error404 from './error404';
import home from './home';
/**
 * Local imports
 */
import AppController from './app.controller';
import routing from './app.routes';

export default angular.module('app', [
  config,
  ngCookies,
  ngSanitize,
  ngAria,
  ngMessages,
  ngMaterial,
  uiRouter,
  angularElastic,
  base,
  error404,
  home
]).config(routing)
    .controller('AppController', AppController)
    .name;
