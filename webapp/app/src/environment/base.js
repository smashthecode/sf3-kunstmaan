export default {
  defaultLang: 'en',
  translate: true,
  api: {
    baseUrl: '/api'
  },
  upload: {
    baseUrl: '/uploads/'
  }
};
