export default ngInject(RestangularProvider => {
  RestangularProvider.setDefaultHeaders({
    'Content-Type': 'application/json'
  });
});
