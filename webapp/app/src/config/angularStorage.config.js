export default ngInject((storeProvider) => {
  storeProvider.setStore('localStorage');
});
