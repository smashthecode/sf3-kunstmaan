import angular from 'angular';
import translate from 'angular-translate';
import angularStorage from 'angular-storage';
import translateLocalStorage from 'angular-translate-storage-local';
import translateCookieStorage from 'angular-translate-storage-cookie';
import ngMaterial from 'angular-material';
import 'restangular';

import api from '../api';

import LiteralLanguageLoader from './literalLanguageLoader.service';
import angularStorageConfig from './angularStorage.config';
import angularDebugConfig from './angularDebug.config';
import restangularConfig from './restangular.config';
// import ConfigService from './config.service';

export default angular.module('config', [
  api,
  translate,
  translateLocalStorage,
  translateCookieStorage,
  angularStorage,
  ngMaterial
])
  .config(angularStorageConfig)
  .config(angularDebugConfig)
  .config(restangularConfig)
  .factory('LiteralLanguageLoader', LiteralLanguageLoader)
  .name;
