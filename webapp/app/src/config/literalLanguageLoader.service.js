import envConfig from 'env-config';

export default ngInject(function LiteralLanguageLoader($q, $http) {
  return (options) => {
    let langFile;
    if (options.key && options.key in options.langs) {
      langFile = options.langs[options.key];
    } else {
      langFile = options.langs[envConfig.defaultLang];
    }

    return $http(angular.extend({
      url: langFile,
      method: 'GET',
      params: ''
    }, options.$http))
      .then((result) => {
        return result.data;
      }, () => {
        return $q.reject(options.key);
      });
  };
});
