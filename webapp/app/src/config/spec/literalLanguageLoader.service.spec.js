import angular from 'angular';
import envConfig from 'env-config';

import configModule from '../index';

describe('Service: LiteralLanguageLoader', () => {
  let $rootScope, literalLanguageLoader, $httpBackend, exampleOptions;

  beforeEach(angular.mock.module(configModule));

  beforeEach(angular.mock.inject((_$httpBackend_) => {
    $httpBackend = _$httpBackend_;
  }));

  beforeEach(angular.mock.inject((_$rootScope_, _LiteralLanguageLoader_) => {
    $rootScope = _$rootScope_;
    literalLanguageLoader = _LiteralLanguageLoader_;

    exampleOptions = {
      langs: {
        en: '/public/languages/en.json',
        pl: '/public/languages/pl.json'
      },
      key: 'en'
    };
  }));

  it('should send request to get language file', (done) => {
    $httpBackend.expectGET('/public/languages/en.json').respond(200, {prop: 'value'});
    literalLanguageLoader(exampleOptions).then((config) => {
      expect(config).toEqual({prop: 'value'});
      done();
    });
    $httpBackend.flush();
    $rootScope.$digest();
  });

  it('should return language code when file does not exist', (done) => {
    $httpBackend.expectGET('/public/languages/en.json').respond(404, {});
    literalLanguageLoader(exampleOptions).catch((config) => {
      expect(config).toEqual('en');
      done();
    });
    $httpBackend.flush();
    $rootScope.$digest();
  });

  it('should not try to get non-existent language', (done) => {
    exampleOptions.key = 'non-existent_language';
    $httpBackend.expectGET(`/public/languages/${envConfig.defaultLang}.json`).respond(200, {prop: 'value'});
    literalLanguageLoader(exampleOptions).then((config) => {
      expect(config).toEqual({prop: 'value'});
      done();
    });
    $httpBackend.flush();
    $rootScope.$digest();
  });
});
