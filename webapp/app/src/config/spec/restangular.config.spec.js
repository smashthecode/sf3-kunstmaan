import restangularConfig from '../restangular.config';

describe('Config: restangular', () => {
  let restangularProvider;

  beforeEach(() => {
    restangularProvider = {
      setDefaultHeaders() {return this;}
    };
  });

  it('should set default content type', () => {
    spyOn(restangularProvider, 'setDefaultHeaders');

    restangularConfig(restangularProvider);

    expect(restangularProvider.setDefaultHeaders).toHaveBeenCalledWith(jasmine.any(Object));
    expect(Object.keys(restangularProvider.setDefaultHeaders.calls.argsFor(0)[0])).toEqual(['Content-Type']);
  });
});
