import angularStorageConfig from '../angularStorage.config';

describe('Config: angularStorage', () => {
  it('should set local storage as default store provider', () => {
    let store = {
      setStore() {}
    };
    spyOn(store, 'setStore');

    angularStorageConfig(store);

    expect(store.setStore).toHaveBeenCalledWith('localStorage');
  });
});
