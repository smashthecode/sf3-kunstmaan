import envConfig from 'env-config';

export default ngInject(($compileProvider) => {
  $compileProvider.debugInfoEnabled(envConfig.angularDebugInfo);
});
