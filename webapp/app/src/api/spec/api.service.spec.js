import angular from 'angular';

import apiModule from '../index';

describe('Service: API', () => {
  let Restangular;

  beforeEach(angular.mock.module(apiModule));

  beforeEach(angular.mock.inject((_Restangular_) => {
    Restangular = _Restangular_;
  }));

  it('should call a Restangular service', () => {
    spyOn(Restangular, 'withConfig');

    inject(function ($injector) {
      $injector.get('API');
    });

    expect(Restangular.withConfig).toHaveBeenCalled();
  });

  it('should configure a Restangular service', () => {
    let configurerMock = {
      setBaseUrl() {}
    };
    spyOn(configurerMock, 'setBaseUrl');
    Restangular.withConfig = (fn) => {
      return fn(configurerMock);
    };

    inject(function ($injector) {
      $injector.get('API');
    });

    expect(configurerMock.setBaseUrl).toHaveBeenCalledWith('/api');
  });
});
