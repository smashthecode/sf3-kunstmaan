import envConfig from 'env-config';

export default ngInject(function API(Restangular) {
  /**
   * Handling abstract layer of Restangular
   *
   * @param {Object}
   */
  return Restangular.withConfig(RestangularConfigurer => {
    RestangularConfigurer.setBaseUrl(envConfig.api.baseUrl);
  });
});
