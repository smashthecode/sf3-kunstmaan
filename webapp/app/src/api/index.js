import angular from 'angular';
import 'restangular';

import Api from './api.service';

export default angular.module('api', [
  'restangular'
]).service('API', Api)
  .name;
