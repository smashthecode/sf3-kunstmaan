/**
 * vendor styles
 */
require('normalize.css/normalize.css');
require('angular-material/angular-material.css');
require('./src/main.scss');

/**
 * ng-annotate helper
 */
window.ngInject = (fn) => fn;

/**
 * Global jquery to replace angular's jqLite
 */
window.jQuery = window.$ = require('jquery');

/**
 * Make sure angular is required before other node modules,
 * which often have imports with side effects
 */
require('lodash');
require('angular');

require('gsap');

require('!modernizr!../.modernizrrc');

/**
 * Include your application
 */
require('./src/app');
