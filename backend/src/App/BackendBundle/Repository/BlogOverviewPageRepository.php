<?php

namespace App\BackendBundle\Repository;

use Kunstmaan\ArticleBundle\Repository\AbstractArticleOverviewPageRepository;

/**
 * Repository class for the BlogOverviewPage
 */
class BlogOverviewPageRepository extends AbstractArticleOverviewPageRepository
{
}
