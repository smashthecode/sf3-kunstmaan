<?php

namespace App\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kunstmaan\ArticleBundle\Entity\AbstractAuthor;
use App\BackendBundle\Form\BlogAuthorAdminType;
use Symfony\Component\Form\AbstractType;

/**
 * The author for a Blog
 *
 * @ORM\Entity()
 * @ORM\Table(name="app_backend_bundle_blog_authors")
 */
class BlogAuthor extends AbstractAuthor
{
    /**
     * Returns the default backend form type for this page
     *
     * @return AbstractType
     */
    public function getAdminType()
    {
        return new BlogAuthorAdminType();
    }
}