<?php

namespace App\BackendBundle\Entity\Pages;

use App\BackendBundle\Form\Pages\HomePageAdminType;
use Doctrine\ORM\Mapping as ORM;
use Kunstmaan\NodeBundle\Entity\AbstractPage;
use Kunstmaan\NodeBundle\Entity\HomePageInterface;
use Kunstmaan\NodeSearchBundle\Helper\SearchTypeInterface;
use Kunstmaan\PagePartBundle\Helper\HasPageTemplateInterface;
use Symfony\Component\Form\AbstractType;

/**
 * HomePage
 * @ORM\Entity()
 * @ORM\Table(name="app_backend_bundle_home_pages")
 */
class HomePage extends AbstractPage implements HasPageTemplateInterface, SearchTypeInterface, HomePageInterface
{
    /**
     * Returns the default backend form type for this page
     * @return AbstractType
     */
    public function getDefaultAdminType()
    {
        return new HomePageAdminType();
    }

    /**
     * @return array
     */
    public function getPossibleChildTypes()
    {
        return array(
            array(
                'name'  => 'ContentPage',
                'class' => 'App\BackendBundle\Entity\Pages\ContentPage',
            ),
            array(
                'name'  => 'FormPage',
                'class' => 'App\BackendBundle\Entity\Pages\FormPage',
            ),
            array(
                'name'  => 'BlogOverviewPage',
                'class' => 'App\BackendBundle\Entity\Pages\BlogOverviewPage',
            ),
        );
    }

    /**
     * @return string[]
     */
    public function getPagePartAdminConfigurations()
    {
        return array(
            'AppBackendBundle:header',
            'AppBackendBundle:section1',
            'AppBackendBundle:section2',
            'AppBackendBundle:section3',
            'AppBackendBundle:section4',
            'AppBackendBundle:section5',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getPageTemplates()
    {
        return array('AppBackendBundle:homepage');
    }

    /**
     * @return string
     */
    public function getDefaultView()
    {
        return 'AppBackendBundle:Pages\HomePage:view.html.twig';
    }

    /**
     * {@inheritdoc}
     */
    public function getSearchType()
    {
        return 'Home';
    }
}
