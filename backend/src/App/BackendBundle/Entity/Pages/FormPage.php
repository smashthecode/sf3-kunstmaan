<?php

namespace App\BackendBundle\Entity\Pages;

use App\BackendBundle\Form\Pages\FormPageAdminType;
use Doctrine\ORM\Mapping as ORM;
use Kunstmaan\FormBundle\Entity\AbstractFormPage;
use Kunstmaan\PagePartBundle\Helper\HasPageTemplateInterface;
use Symfony\Component\Form\AbstractType;

/**
 * FormPage
 *
 * @ORM\Entity()
 * @ORM\Table(name="app_backend_bundle_form_pages")
 */
class FormPage extends AbstractFormPage implements HasPageTemplateInterface
{
    /**
     * Returns the default backend form type for this form
     *
     * @return AbstractType
     */
    public function getDefaultAdminType()
    {
        return new FormPageAdminType();
    }

    /**
     * @return array
     */
    public function getPossibleChildTypes()
    {
        return array(
            array(
                'name'  => 'ContentPage',
                'class' => 'App\BackendBundle\Entity\Pages\ContentPage',
            ),
            array(
                'name'  => 'FormPage',
                'class' => 'App\BackendBundle\Entity\Pages\FormPage',
            ),
        );
    }

    /**
     * @return string[]
     */
    public function getPagePartAdminConfigurations()
    {
        return array('AppBackendBundle:form');
    }

    /**
     * {@inheritdoc}
     */
    public function getPageTemplates()
    {
        return array('AppBackendBundle:formpage');
    }

    /**
     * @return string
     */
    public function getDefaultView()
    {
        return 'AppBackendBundle:Pages\FormPage:view.html.twig';
    }
}
