<?php

namespace App\BackendBundle\Entity\Pages;

use Doctrine\ORM\Mapping as ORM;
use Kunstmaan\ArticleBundle\Entity\AbstractArticlePage;
use Kunstmaan\NodeSearchBundle\Helper\SearchTypeInterface;
use Kunstmaan\PagePartBundle\Helper\HasPageTemplateInterface;
use App\BackendBundle\Entity\BlogAuthor;
use App\BackendBundle\Form\Pages\BlogPageAdminType;
use Symfony\Component\Form\AbstractType;

/**
 * @ORM\Entity(repositoryClass="App\BackendBundle\Repository\BlogPageRepository")
 * @ORM\Table(name="app_backend_bundle_blog_pages")
 * @ORM\HasLifecycleCallbacks
 */
class BlogPage extends AbstractArticlePage implements HasPageTemplateInterface, SearchTypeInterface
{
    /**
     * @var BlogAuthor
     *
     * @ORM\ManyToOne(targetEntity="App\BackendBundle\Entity\BlogAuthor")
     * @ORM\JoinColumn(name="blog_author_id", referencedColumnName="id")
     */
    protected $author;

    /**
     * @param BlogAuthor $author
     *
     * @return $this
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return BlogAuthor
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Returns the default backend form type for this page
     *
     * @return AbstractType
     */
    public function getDefaultAdminType()
    {
        return new BlogPageAdminType();
    }

    /**
     * {@inheritdoc}
     */
    public function getSearchType()
    {
        return 'Blog';
    }

    /**
     * @return array
     */
    public function getPagePartAdminConfigurations()
    {
        return array('AppBackendBundle:main');
    }

    /**
     * {@inheritdoc}
     */
    public function getPageTemplates()
    {
        return array('AppBackendBundle:blogpage');
    }

    /**
     * @return string
     */
    public function getDefaultView()
    {
        return 'AppBackendBundle:Pages/BlogPage:view.html.twig';
    }

    /**
     * Before persisting this entity, check the date.
     * When no date is present, fill in current date and time.
     *
     * @ORM\PrePersist
     */
    public function _prePersist()
    {
        // Set date to now when none is set
        if ($this->date == null) {
            $this->setDate(new \DateTime());
        }
    }
}
