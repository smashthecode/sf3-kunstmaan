<?php

namespace App\BackendBundle\Entity\Pages;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Kunstmaan\ArticleBundle\Repository\AbstractArticlePageRepository;
use Kunstmaan\NodeSearchBundle\Helper\SearchTypeInterface;
use Kunstmaan\PagePartBundle\Helper\HasPageTemplateInterface;
use App\BackendBundle\Form\Pages\BlogOverviewPageAdminType;
use Kunstmaan\ArticleBundle\Entity\AbstractArticleOverviewPage;
use Kunstmaan\PagePartBundle\PagePartAdmin\AbstractPagePartAdminConfigurator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * The article overview page which shows its articles
 *
 * @ORM\Entity(repositoryClass="App\BackendBundle\Repository\BlogOverviewPageRepository")
 * @ORM\Table(name="app_backend_bundle_blog_overview_pages")
 */
class BlogOverviewPage extends AbstractArticleOverviewPage implements HasPageTemplateInterface, SearchTypeInterface
{
    /**
     * @return AbstractPagePartAdminConfigurator[]
     */
    public function getPagePartAdminConfigurations()
    {
        return array('AppBackendBundle:main');
    }

    /**
     * {@inheritdoc}
     */
    public function getPageTemplates()
    {
        return array('AppBackendBundle:blogoverviewpage');
    }

    /**
     * @param EntityManager $em
     *
     * @return AbstractArticlePageRepository
     */
    public function getArticleRepository($em)
    {
        return $em->getRepository('AppBackendBundle:Pages\BlogPage');
    }

    /**
     * @return string
     */
    public function getDefaultView()
    {
        return 'AppBackendBundle:Pages/BlogOverviewPage:view.html.twig';
    }

    /**
     * {@inheritdoc}
     */
    public function getSearchType()
    {
        return 'Blog';
    }

    /**
     * Returns the default backend form type for this page
     *
     * @return AbstractType
     */
    public function getDefaultAdminType()
    {
        return new BlogOverviewPageAdminType();
    }
}
