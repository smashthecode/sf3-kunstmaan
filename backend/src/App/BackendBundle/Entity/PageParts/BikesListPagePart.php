<?php

namespace App\BackendBundle\Entity\PageParts;

use Doctrine\ORM\Mapping as ORM;

/**
 * BikesListPagePart
 *
 * @ORM\Table(name="app_backend_bundle_bikes_list_page_parts")
 * @ORM\Entity
 */
class BikesListPagePart extends AbstractPagePart
{
    /**
     * Get the twig view.
     *
     * @return string
     */
    public function getDefaultView()
    {
        return 'AppBackendBundle:PageParts:BikesListPagePart/view.html.twig';
    }

    /**
     * Get the admin form type.
     *
     * @return \App\BackendBundle\Form\PageParts\BikesListPagePartAdminType
     */
    public function getDefaultAdminType()
    {
        return new \App\BackendBundle\Form\PageParts\BikesListPagePartAdminType();
    }
}
