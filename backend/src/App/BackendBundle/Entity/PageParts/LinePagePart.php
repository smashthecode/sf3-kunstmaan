<?php

namespace App\BackendBundle\Entity\PageParts;

use Doctrine\ORM\Mapping as ORM;

/**
 * LinePagePart
 *
 * @ORM\Table(name="app_backend_bundle_line_page_parts")
 * @ORM\Entity
 */
class LinePagePart extends AbstractPagePart
{
    /**
     * Get the twig view.
     *
     * @return string
     */
    public function getDefaultView()
    {
        return 'AppBackendBundle:PageParts:LinePagePart/view.html.twig';
    }

    /**
     * Get the admin form type.
     *
     * @return \App\BackendBundle\Form\PageParts\LinePagePartAdminType
     */
    public function getDefaultAdminType()
    {
        return new \App\BackendBundle\Form\PageParts\LinePagePartAdminType();
    }
}
