<?php

namespace App\BackendBundle\Entity\PageParts;

use Doctrine\ORM\Mapping as ORM;

/**
 * TocPagePart
 *
 * @ORM\Table(name="app_backend_bundle_toc_page_parts")
 * @ORM\Entity
 */
class TocPagePart extends AbstractPagePart
{
    /**
     * Get the twig view.
     *
     * @return string
     */
    public function getDefaultView()
    {
        return 'AppBackendBundle:PageParts:TocPagePart/view.html.twig';
    }

    /**
     * Get the admin form type.
     *
     * @return \App\BackendBundle\Form\PageParts\TocPagePartAdminType
     */
    public function getDefaultAdminType()
    {
        return new \App\BackendBundle\Form\PageParts\TocPagePartAdminType();
    }
}
