var AppBackendBundle = AppBackendBundle || {};

AppBackendBundle = (function($, window, undefined) {

    var init;

    init = function() {
        cargobay.scrollToTop.init();
        cargobay.toggle.init();
        cargobay.sidebarToggle.init();
        cargobay.videolink.init();
        cargobay.cookieConsent.init();
        AppBackendBundle.search.init();
        AppBackendBundle.demoMsg.init();
    };

    return {
        init: init
    };

}(jQuery, window));

$(function() {
    AppBackendBundle.init();
});
