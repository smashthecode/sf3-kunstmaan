<?php

namespace App\BackendBundle\Form\Pages;

use Kunstmaan\ArticleBundle\Form\AbstractArticleOverviewPageAdminType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The admin type for Blogoverview pages
 */
class BlogOverviewPageAdminType extends AbstractArticleOverviewPageAdminType
{
    /**
     * Sets the default options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\BackendBundle\Entity\Pages\BlogOverviewPage',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'blog_overview_page_type';
    }
}
