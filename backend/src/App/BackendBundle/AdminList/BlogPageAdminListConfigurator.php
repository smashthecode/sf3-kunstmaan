<?php

namespace App\BackendBundle\AdminList;

use Doctrine\ORM\QueryBuilder;
use Kunstmaan\ArticleBundle\AdminList\AbstractArticlePageAdminListConfigurator;

/**
 * The AdminList configurator for the BlogPage
 */
class BlogPageAdminListConfigurator extends AbstractArticlePageAdminListConfigurator
{
    /**
     * Return current bundle name.
     *
     * @return string
     */
    public function getBundleName()
    {
        return 'AppBackendBundle';
    }

    /**
     * Return current entity name.
     *
     * @return string
     */
    public function getEntityName()
    {
        return 'Pages\BlogPage';
    }

    /**
     * @param QueryBuilder $queryBuilder The query builder
     */
    public function adaptQueryBuilder(QueryBuilder $queryBuilder)
    {
        parent::adaptQueryBuilder($queryBuilder);

        $queryBuilder->setParameter('class', 'App\BackendBundle\Entity\Pages\BlogPage');
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getOverviewPageRepository()
    {
        return $this->em->getRepository('AppBackendBundle:Pages\BlogOverviewPage');
    }

    /**
     * @return string
     */
    public function getListTemplate()
    {
        return 'AppBackendBundle:AdminList/BlogPageAdminList:list.html.twig';
    }
}
