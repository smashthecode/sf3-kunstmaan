<?php

namespace App\GeneralBundle\Repository\TranslatorBundle;

use Doctrine\ORM\EntityManager;

/**
 * TranslationRepository
 */
class TranslationRepository
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var \Kunstmaan\TranslatorBundle\Repository\TranslationRepository
     */
    private $translationRepository;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->translationRepository = $em->getRepository('KunstmaanTranslatorBundle:Translation');
    }

    /**
     * @param $locale
     *
     * @return array
     */
    public function getTranslationsByLocale($locale)
    {
        $qb = $this->em->createQueryBuilder();

        return $qb
            ->select('t.keyword,t.text,t.domain')
            ->from('KunstmaanTranslatorBundle:Translation', 't')
            ->andWhere('t.locale = :locale')
            ->setParameter('locale', $locale)
            ->getQuery()
            ->getArrayResult();
    }
}
