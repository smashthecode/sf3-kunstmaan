<?php

namespace App\GeneralBundle\Twig;

use Twig_Extension;
use Twig_Environment;
use Doctrine\ORM\EntityManager;
use Kunstmaan\SeoBundle\Entity\Seo;

/**
 * Twig extensions for Seo
 */
class SeoTwigExtension extends Twig_Extension
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Returns a list of functions to add to the existing list.
     * @return array An array of functions
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                'general_render_seo_metadata_for',
                [$this, 'renderSeoMetadataFor'],
                [
                    'is_safe'           => ['html'],
                    'needs_environment' => true,
                ]
            ),
        ];
    }

    /**
     * @param \Twig_Environment $environment
     * @param Seo               $seo         Seo
     * @param mixed             $currentNode The current node
     * @param bool              $api         render by api
     * @param string            $template    The template
     *
     * @return string
     */
    public function renderSeoMetadataFor(
        \Twig_Environment $environment,
        Seo $seo,
        $currentNode = null,
        $api = false,
        $template = 'AppBackendBundle:Layout:_metadata.html.twig'
    ) {
        $template = $environment->loadTemplate($template);

        return $template->render(
            [
                'seo'         => $seo,
                'currentNode' => $currentNode,
                'api'         => $api,
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'general_seo_twig_extension';
    }
}
