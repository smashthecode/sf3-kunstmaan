<?php
namespace App\GeneralBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 */
abstract class BaseController extends FOSRestController
{
    /**
     * @param array $data
     *
     * @return Response
     */
    public function handleBadResponseAsErrors($data)
    {
        return $this->handleView($this->view(['errors' => $data], Response::HTTP_BAD_REQUEST));
    }

    /**
     * @param array|object|null $data
     *
     * @return Response
     */
    public function handleBadResponse($data = null)
    {
        return $this->handleView($this->view($data, Response::HTTP_BAD_REQUEST));
    }

    /**
     * @param array|object|null $data
     *
     * @return Response
     */
    public function handleForbiddenResponse($data = null)
    {
        return $this->handleView($this->view($data, Response::HTTP_FORBIDDEN));
    }

    /**
     * @param null $data
     *
     * @return Response
     */
    public function handle404Response($data = null)
    {
        return $this->handleView($this->view($data, Response::HTTP_NOT_FOUND));
    }

    /**
     * @param array|object|null $data
     *
     * @return Response
     */
    public function handleOkResponse($data = null)
    {
        return $this->handleView($this->view($data, Response::HTTP_OK));
    }

    /**
     * @param Request $request
     * @param string $formName
     * @param string $namespace
     * @param object|null $formData
     * @param int $statusCode
     *
     * @return Response|Form
     */
    protected function processStandardForm(
        Request $request,
        $formName,
        $namespace,
        $formData = null,
        $statusCode = Response::HTTP_OK
    ) {
        $form = $this->createForm($formName, $formData);
        $post = array_merge($request->get($form->getName(), []), $request->files->all());
        $clearMissing = ($request->isMethod('PATCH') ? false : true);
        $form->submit($post, $clearMissing);
        $handler = $this->get(sprintf('%s.form.handler.%s', $namespace, $form->getName()));

        $result = $handler->process($form);
        if ($result) {
            return $this->handleView($this->view($result === true ? null : $result, $statusCode));
        }

        return $this->handleBadResponse($form->getErrors());
    }
}