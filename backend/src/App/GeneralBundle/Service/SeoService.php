<?php

namespace App\GeneralBundle\Service;

use Kunstmaan\AdminBundle\Entity\AbstractEntity;
use Kunstmaan\MediaBundle\Entity\Media;
use Kunstmaan\SeoBundle\Repository\SeoRepository;
use Kunstmaan\SeoBundle\Entity\Seo;

/**
 * @package App\GeneralBundle\Service
 */
class SeoService
{
    /**
     * @var SeoRepository
     */
    private $seoRepository;

    /**
     * @var MediaService
     */
    private $mediaService;

    /**
     * SeoService constructor.
     *
     * @param SeoRepository $seoRepository
     * @param MediaService  $mediaService
     */
    public function __construct(SeoRepository $seoRepository, MediaService $mediaService)
    {
        $this->seoRepository = $seoRepository;
        $this->mediaService = $mediaService;
    }

    /**
     * @param AbstractEntity $entity
     *
     * @return null|Seo
     */
    public function getSeoForEntity(AbstractEntity $entity)
    {
        return $this->seoRepository->findFor($entity);
    }

    /**
     * @param AbstractEntity $currentEntity
     * @param AbstractEntity $homepageEntity
     *
     * @return array|null
     */
    public function getMergedSeoWithHomepage(AbstractEntity $currentEntity, AbstractEntity $homepageEntity)
    {
        $currentEntitySeo = $this->seoRepository->findFor($currentEntity);
        $homepageEntitySeo = $this->seoRepository->findFor($homepageEntity);

        if (!$currentEntitySeo) {
            return $homepageEntitySeo;
        }

        if (!$currentEntitySeo && !$homepageEntitySeo) {
            return null;
        }

        $seo = [];

        $methods = [
            'metaTitle',
            'metaDescription',
            'metaRobots',
            'extraMetadata',
            'ogType',
            'ogTitle',
            'ogDescription',
            'ogImage',
            'ogUrl',
            'ogArticleAuthor',
            'ogArticlePublisher',
            'ogArticleSection',
            'twitterTitle',
            'twitterDescription',
            'twitterSite',
            'twitterCreator',
            'twitterImage',
        ];

        $mergedSeo = new Seo();

        foreach ($methods as $method) {
            $ucMethod = ucfirst($method);
            $setMethod = 'set'.$ucMethod;
            $getMethod = 'get'.$ucMethod;
            $mergedSeo->$setMethod($this->getSeoTagByMethod($currentEntitySeo, $getMethod));

            if (!$mergedSeo->$getMethod()) {
                $mergedSeo->$setMethod($this->getSeoTagByMethod($homepageEntitySeo, $getMethod));
            }
        }

        return $mergedSeo;
    }

    /**
     * @param object $seo
     * @param string $getMethod
     *
     * @return array
     */
    public function getSeoTagByMethod($seo, $getMethod)
    {
        if ($seo instanceof Seo && method_exists($seo, $getMethod)) {
            return $seo->$getMethod();
        }

        return null;
    }
}
