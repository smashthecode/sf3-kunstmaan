<?php

namespace App\GeneralBundle\Service;

use Kunstmaan\AdminBundle\Entity\AbstractEntity;
use Kunstmaan\MediaBundle\Entity\Media;
use Kunstmaan\SeoBundle\Repository\SeoRepository;
use Kunstmaan\SeoBundle\Entity\Seo;

/**
 * @package App\GeneralBundle\Service
 */
class MediaService
{
    /**
     * @param Media|null $media
     *
     * @return array
     */
    public function prepareMedia($media)
    {
        return $media ? [
            'url'         => $media->getUrl(),
            'location'    => $media->getLocation(),
            'contentType' => $media->getContentType(),
        ] : null;
    }
}
