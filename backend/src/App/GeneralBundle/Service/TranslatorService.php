<?php

namespace App\GeneralBundle\Service;

use App\GeneralBundle\Repository\TranslatorBundle\TranslationRepository;


/**
 * @package App\GeneralBundle\Service
 */
class TranslatorService
{
    /**
     * @var TranslationRepository
     */
    private $translationRepository;

    /**
     * @var string
     */
    private $defaultLocale;

    /**
     * TranslatorService constructor.
     *
     * @param TranslationRepository $translatorRepository
     * @param                       $defaultLocale
     */
    public function __construct(TranslationRepository $translatorRepository, $defaultLocale)
    {
        $this->translationRepository = $translatorRepository;
        $this->defaultLocale = $defaultLocale;
    }

    /**
     * @param $locale
     *
     * @return array
     */
    public function getDefaultTranslationsForLocale($locale)
    {
        $translationsByLocale = $this->translationRepository->getTranslationsByLocale($locale);
        $defaultTranslations = $this->translationRepository->getTranslationsByLocale($this->defaultLocale);

        $translations = [];
        foreach ($translationsByLocale as $translation) {
            $translations[$translation['keyword']] = $translation;
        }

        foreach ($defaultTranslations as $translation) {
            if (!isset($translations[$translation['keyword']])) {
                $translations[$translation['keyword']] = $translation;
            }
        }

        return array_values($translations);
    }
}
