<?php

namespace App\GeneralBundle\Service;

use App\GeneralBundle\Repository\NodeBundle\NodeRepository;
use Doctrine\ORM\EntityManager;
use Kunstmaan\AdminBundle\Entity\AbstractEntity;
use Kunstmaan\AdminBundle\Helper\Security\Acl\Permission\PermissionMap;
use Kunstmaan\ArticleBundle\Entity\AbstractArticleOverviewPage;
use Kunstmaan\NodeBundle\Entity\HasNodeInterface;
use Kunstmaan\NodeBundle\Entity\Node;
use Kunstmaan\NodeBundle\Entity\NodeTranslation;
use Kunstmaan\NodeBundle\Entity\NodeVersion;
use Kunstmaan\NodeBundle\Repository\NodeTranslationRepository;
use Kunstmaan\PagePartBundle\Entity\PageTemplateConfiguration;
use Kunstmaan\PagePartBundle\PageTemplate\PageTemplate;
use Kunstmaan\PagePartBundle\PageTemplate\PageTemplateConfigurationReader;
use Kunstmaan\PagePartBundle\PageTemplate\Row;
use Kunstmaan\PagePartBundle\Repository\PageTemplateConfigurationRepository;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;

/**
 * @package App\GeneralBundle\Service
 */
class NodeService
{
    /**
     * @var NodeRepository
     */
    private $nodeRepository;

    /**
     * @var NodeTranslationRepository
     */
    private $nodeTranslationRepository;

    /**
     * @var PageTemplateConfigurationRepository
     */
    private $pageTemplateConfigurationRepository;

    /**
     * @var PageTemplateConfigurationReader
     */
    private $pageTemplateConfigurationReader;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param NodeRepository                  $nodeRepository
     * @param PageTemplateConfigurationReader $pageTemplateConfigurationReader
     * @param EntityManager                   $em
     */
    public function __construct(
        NodeRepository $nodeRepository,
        PageTemplateConfigurationReader $pageTemplateConfigurationReader,
        EntityManager $em
    ) {
        $this->nodeRepository = $nodeRepository;
        $this->nodeTranslationRepository = $em->getRepository('KunstmaanNodeBundle:NodeTranslation');
        $this->pageTemplateConfigurationRepository = $em
            ->getRepository('KunstmaanPagePartBundle:PageTemplateConfiguration');
        $this->pageTemplateConfigurationReader = $pageTemplateConfigurationReader;
        $this->em = $em;
    }

    /**
     * @param $locale
     *
     * @return array
     */
    public function getNodesForMenu($locale)
    {
        $excluded = [
            'App\BackendBundle\Entity\Pages\BlogPage',
        ];

        $nodes = $this->nodeRepository->getAllMenuNodes($locale, PermissionMap::PERMISSION_VIEW, $excluded);

        $activeNodes = [];
        foreach ($nodes as $key => $node) {
            if ($node['online']) {
                $node['entity_name'] = null;
                preg_match('/(\w+\\\\\w+)\Z/', $node['ref_entity_name'], $matches);
                if ($matches) {
                    $node['entity_name'] = mb_strtolower($matches[1]);
                }

                unset(
                    $node['ref_entity_name'],
                    $node['nt'],
                    $node['online']
                );
                $activeNodes[] = $node;
            }
        }

        return $activeNodes;
    }

    /**
     * @param Node $node
     *
     * @return bool
     */
    public function isAuthenticated(Node $node)
    {
        return $this->nodeRepository->isAuthenticated($node, PermissionMap::PERMISSION_VIEW);
    }

    /**
     * @param object $entity
     * @param string $locale
     * @param int    $limit
     * @param int    $page
     *
     * @return null|Pagerfanta
     */
    public function getPagerfantaForList($entity, $locale, $limit, $page)
    {
        if ($entity instanceof AbstractArticleOverviewPage) {
            $repository = $entity->getArticleRepository($this->em);
            $adapter = new DoctrineORMAdapter($repository->getArticlesQb($locale));
            $pagerfanta = new Pagerfanta($adapter);
            $pagerfanta
                ->setMaxPerPage($limit)
                ->setCurrentPage($page);

            return $pagerfanta;
        }

        return null;
    }


    /**
     * @param Request $request
     * @param boolean $preview
     * @param NodeTranslation $nodeTranslation
     *
     * @return AbstractEntity
     */
    public function getPageEntity(
        Request $request,
        $preview,
        NodeTranslation $nodeTranslation
    ) {
        /* @var HasNodeInterface $entity */
        $entity = null;
        if ($preview) {
            $version = $request->get('version');
            if (!empty($version) && is_numeric($version)) {
                /** @var NodeVersion $nodeVersion */
                $nodeVersion = $this->em->getRepository('KunstmaanNodeBundle:NodeVersion')->find($version);
                if (!is_null($nodeVersion)) {
                    $entity = $nodeVersion->getRef($this->em);
                }
            }
        }
        if (is_null($entity)) {
            $entity = $nodeTranslation->getPublicNodeVersion()->getRef($this->em);

            return $entity;
        }

        return $entity;
    }

    /**
     * @param $template
     *
     * @return mixed
     */
    public function getJsonFromTemplate($template)
    {
        $template = preg_replace('/\s{2,}/', '', $template);
        $template = sprintf("<body>%s</body>", $template);

        $xml = simplexml_load_string($template);
        $json = json_encode($xml);

        return json_decode($json, true);
    }

    /**
     * @param $url
     * @param $locale
     *
     * @return NodeTranslation|null
     */
    public function getNodeTranslationByUrl($url, $locale)
    {
        return $this->nodeTranslationRepository->getNodeTranslationForUrl($url, $locale);
    }

    /**
     * @param $page
     *
     * @return array
     * @throws \Exception
     */
    public function getPageConfigForPage($page)
    {
        $config = $this->pageTemplateConfigurationRepository->findFor($page);
        $pages = $this->pageTemplateConfigurationReader->getPageTemplates($page);

        /** @var PageTemplate $pageConfig */
        $pageConfig = $config ? $pages[$config->getPageTemplate()] : null;
        $pageParts = [];


        /** TODO get page config for a homepage */
        if ($pageConfig) {
            /** @var Row $row */
            foreach ($pageConfig->getRows() as $row) {
                foreach ($row->getRegions() as $region) {
                    $pageParts[] = [
                        'name' => $region->getName(),
                        'span' => $region->getSpan(),
                    ];
                }
            }
        }

        return $pageParts;
    }
}
