<?php
namespace App\GeneralBundle\Tests\Base;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractBaseWebTestCase extends WebTestCase
{
    /**
     * run code before each test
     */
    public function setUp()
    {
        $this->getContainer()->get('doctrine.orm.default_entity_manager')->clear();
    }

    /**
     * run code after each test
     */
    public function tearDown()
    {
        $this->getContainer()->get('doctrine.orm.default_entity_manager')->clear();
    }

    /**
     * Create a client with a default Authorization header.
     * When a password is provided then a POST request will be performed in order to log in a user.
     *
     * @param string|null $username
     * @param string|null $password
     *
     * @return Client|bool
     */
    protected function createAuthenticatedClient($username = null, $password = null)
    {
        if (!$username) {
            $username = 'admin@admin.pl';
        }

        if ($password) {
            $client = static::createClient();
            $client->request(
                'POST',
                $this->getUrl('api_post_user_security_login'),
                [
                    '_username' => $username,
                    '_password' => $password,
                ],
                [],
                ['CONTENT_TYPE' => 'application/json']
            );

            $this->assertStatusCode(Response::HTTP_OK, $client);
            $this->assertJson($client->getResponse()->getContent());
            $content = $this->getContentAsJson($client);
            $this->assertArrayHasKey('token', $content);
            $token = $content['token'];
        } else {
            $container = $this->getContainer();
            $user = $container->get('api.service.user')->getUserManager()->findUserByEmail($username);
            if (!$user) {
                return false;
            }
            $token = $container->get('api.service.user_token')->getToken($user);
        }

        $client = static::createClient();
        $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $token));

        return $client;
    }

    /**
     * Create a client without authorization.
     *
     * @return Client
     */
    protected function createUnauthenticatedClient()
    {
        return static::createClient();
    }

    /**
     * @param Client $client
     *
     * @return array
     */
    protected function getContentAsJson($client)
    {
        $this->assertJson($client->getResponse()->getContent());

        return json_decode($client->getResponse()->getContent(), true);
    }

    /**
     * @param array $parameters
     * @param string|null $url
     *
     * @return Client
     */
    protected function postAsUnauthenticatedClient(array $parameters, $url = null)
    {
        if (!$url) {
            $url = $this->getEndpoint();
        }

        $client = $this->createUnauthenticatedClient();
        $client->request('POST', $url, $parameters, [], ['CONTENT_TYPE' => 'application/json']);

        return $client;
    }

    /**
     * @param array $parameters
     * @param string|null $url
     * @param string|null $username
     * @param string|null $password
     *
     * @return Client
     */
    protected function postAsAuthenticatedClient(array $parameters, $url = null, $username = null, $password = null)
    {
        if (!$url) {
            $url = $this->getEndpoint();
        }

        $client = $this->createAuthenticatedClient($username, $password);
        $client->request('POST', $url, $parameters, [], ['CONTENT_TYPE' => 'application/json']);

        return $client;
    }

    /**
     * @param array $parameters
     * @param string|null $url
     *
     * @return Client
     */
    protected function patchAsUnauthenticatedClient(array $parameters, $url = null)
    {
        if (!$url) {
            $url = $this->getParameterizedEndpoint($parameters);
            unset($parameters['id']);
        }

        $client = $this->createUnauthenticatedClient();
        $client->request('PATCH', $url, [], [], ['CONTENT_TYPE' => 'application/json'], json_encode($parameters));
        return $client;
    }

    /**
     * @param array $parameters
     * @param string|null $url
     * @param string|null $username
     * @param string|null $password
     *
     * @return Client
     */
    protected function patchAsAuthenticatedClient(array $parameters, $url = null, $username = null, $password = null)
    {
        if (!$url) {
            $url = $this->getParameterizedEndpoint($parameters);
            unset($parameters['id']);
        }

        $client = $this->createAuthenticatedClient($username, $password);
        $client->request('PATCH', $url, [], [], ['CONTENT_TYPE' => 'application/json'], json_encode($parameters));

        return $client;
    }

    /**
     * @param array $parameters
     * @param string|null $url
     *
     * @return Client
     */
    protected function deleteAsUnauthenticatedClient(array $parameters, $url = null)
    {
        if (!$url) {
            $url = $this->getParameterizedEndpoint($parameters);
        }

        $client = $this->createUnauthenticatedClient();
        $client->request('DELETE', $url, [], [], ['CONTENT_TYPE' => 'application/json']);

        return $client;
    }

    /**
     * @param array $parameters
     * @param string|null $url
     * @param string|null $username
     * @param string|null $password
     *
     * @return Client
     */
    protected function deleteAsAuthenticatedClient(array $parameters, $url = null, $username = null, $password = null)
    {
        if (!$url) {
            $url = $this->getParameterizedEndpoint($parameters);
        }

        $client = $this->createAuthenticatedClient($username, $password);
        $client->request('DELETE', $url, [], [], ['CONTENT_TYPE' => 'application/json']);

        return $client;
    }

    /**
     * @param string|null $url
     *
     * @return Client
     */
    protected function getAsUnauthenticatedClient($url = null)
    {
        if (!$url) {
            $url = $this->getEndpoint();
        }

        $client = $this->createUnauthenticatedClient();
        $client->request('GET', $url, [], [], ['CONTENT_TYPE' => 'application/json']);

        return $client;
    }

    /**
     * @param string|null $url
     * @param string|null $username
     * @param string|null $password
     *
     * @return Client
     */
    protected function getAsAuthenticatedClient($url = null, $username = null, $password = null)
    {
        if (!$url) {
            $url = $this->getEndpoint();
        }

        $client = $this->createAuthenticatedClient($username, $password);
        $client->request('GET', $url, [], [], ['CONTENT_TYPE' => 'application/json']);

        return $client;
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function getEndpoint()
    {
        throw new \Exception('Implement the method or pass url parameter to function');
    }

    /**
     * @param array $parameters
     *
     * @return string
     * @throws \Exception
     */
    protected function getParameterizedEndpoint(array $parameters)
    {
        throw new \Exception('Implement the method or pass url parameter to function');
    }
}