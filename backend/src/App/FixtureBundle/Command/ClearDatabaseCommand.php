<?php

namespace App\FixtureBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\OutputInterface;

class ClearDatabaseCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:fixture:clear-database')
            ->setDescription('Rebuild database with fresh fixtures');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $commands = [
            ['doctrine:database:drop', '--env' => $input->getOption('env'), '--force' => true],
            ['doctrine:database:create', '--env' => $input->getOption('env')],
            ['doctrine:schema:create', '--env' => $input->getOption('env')],
            ['doctrine:fixtures:load', '--env' => $input->getOption('env'), '--no-interaction' => true],
        ];

        foreach ($commands as $command) {
            $input = is_array($command) ? new ArrayInput($command) : new StringInput($command);
            $input->setInteractive(false);
            $this->getApplication()->doRun($input, $output);
        }
    }
}