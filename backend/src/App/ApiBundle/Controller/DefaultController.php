<?php

namespace App\ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations\RouteResource;
use App\GeneralBundle\Controller\BaseController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Response;

/**
 * @RouteResource("Default", pluralize=false)
 * @package ApiBundle\Controller
 */
class DefaultController extends BaseController
{
    /**
     * Default action for default controller
     *
     * @ApiDoc(
     *  section="Default resources",
     *  statusCodes={200="OK", 401="Unauthorized"},
     *  tags={"tested" = "green"}
     * )
     *
     * @return Response
     */
    public function getHomepageAction()
    {
        return $this->handleOkResponse(['hello' => 'world']);
    }
}
