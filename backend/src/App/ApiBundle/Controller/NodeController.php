<?php

namespace App\ApiBundle\Controller;

use App\GeneralBundle\Service\NodeService;
use App\GeneralBundle\Service\SeoService;
use App\GeneralBundle\Controller\BaseController;
use App\GeneralBundle\Service\TranslatorService;
use FOS\RestBundle\Controller\Annotations\Get;
use Kunstmaan\AdminBundle\Entity\User;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package ApiBundle\Controller
 */
class NodeController extends BaseController
{
    /**
     * @ApiDoc(
     *  section="Node resources",
     *  statusCodes={200="OK"},
     *  tags={"tested" = "red"}
     * )
     * @param Request $request
     *
     * @return Response
     */
    public function getConfigAction(Request $request)
    {
        $locale = $request->getLocale();

        /** @var TranslatorService $translatorService */
        $translatorService = $this->get('app.general_bundle.service.translator');
        $translations = $translatorService->getDefaultTranslationsForLocale($locale);

        return $this->handleOkResponse([
            'menu'         => $this->get('app.general_bundle.service.node')->getNodesForMenu($locale),
            'translations' => $translations,
        ]);
    }

    /**
     * @ApiDoc(
     *  section="Node resources",
     *  statusCodes={200="OK"},
     *  tags={"tested" = "red"}
     * )
     * @param Request $request
     * @param string  $url
     *
     * @return Response
     * @Get("/nodes/{url}", requirements={"url" = ".*"})
     */
    public function getNodeAction(Request $request, $url)
    {
        //trim slash for homepage
        if ($url == '/') {
            $url = '';
        }

        /** @var NodeService $nodeService */
        $nodeService = $this->get('app.general_bundle.service.node');

        /** @var SeoService $seoService */
        $seoService = $this->get('app.general_bundle.service.seo');

        /** @var User $user */
        $user = $this->getUser();
        $preview = $request->get('preview', false);

        //check if user has access to preview
        if ($preview && $user && !$user->hasRole('ROLE_ADMIN')) {
            if ($request->isXmlHttpRequest()) {
                return $this->handleForbiddenResponse(['message' => 'Forbidden']);
            }

            return $this->redirect('/', 301);
        }

        //find node by url
        $nodeTranslation = $nodeService->getNodeTranslationByUrl($url, $request->getLocale());

        //if we haven't node translation -> 404
        if (!$nodeTranslation || !$nodeTranslation->isOnline() && !$preview) {
            if ($request->isXmlHttpRequest()) {
                return $this->handle404Response(['message' => 'Page not found']);
            }

            return $this->redirect('/', 301);
        }

        //check if user has access to this node
        if (!$nodeService->isAuthenticated($nodeTranslation->getNode())) {
            if ($request->isXmlHttpRequest()) {
                return $this->handleForbiddenResponse(['message' => 'Forbidden']);
            }

            return $this->redirect('/', 301);
        }

        //find current page
        $page = $nodeService->getPageEntity(
            $request,
            $preview,
            $nodeTranslation
        );

        //find homepage page for seo
        $homepage = $nodeTranslation = $nodeService->getNodeTranslationByUrl('', $request->getLocale());
        $homepage = $nodeService->getPageEntity(
            $request,
            $preview,
            $homepage
        );

        //merge seo with homepage seo
        $seo = $seoService->getMergedSeoWithHomepage($page, $homepage);
        $seo = $this->get('app.general_bundle.seo.twig.extension')->renderSeoMetadataFor(
            $this->get('twig'),
            $seo,
            $nodeTranslation,
            $request->isXmlHttpRequest()
        );

        $response = ['seo' => $seo];

        //if call AJAX get all info about template and page parts
        if ($request->isXmlHttpRequest()) {
            $params = [
                'page'       => $page,
                'pagerfanta' => $nodeService->getPagerfantaForList(
                    $page,
                    $request->getLocale(),
                    $request->get('limit', 10),
                    $request->get('page', 1)
                ),
            ];

            $template = $this
                ->get('kunstmaan_pagetemplate.twig.extension')
                ->renderPageTemplate($this->get('twig'), ['api' => true], $page, $params);

            $response = [
                'seo'       => $nodeService->getJsonFromTemplate($seo),
                'template'  => $nodeService->getJsonFromTemplate($template),
                'pageparts' => $nodeService->getPageConfigForPage($page),
            ];
        }

        if ($request->isXmlHttpRequest()) {
            return $this->handleOkResponse($response);
        }

        return $this->render('AppBackendBundle:Layout:layout.html.twig', $response);
    }
}
