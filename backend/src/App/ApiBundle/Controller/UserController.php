<?php

namespace App\ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations\RouteResource;
use App\GeneralBundle\Controller\BaseController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * @RouteResource("User", pluralize=false)
 * @package ApiBundle\Controller
 */
class UserController extends BaseController
{
    /**
     * Login user by username/email and password, and return JWT token
     *
     * @ApiDoc(
     *  section="User resources",
     *  statusCodes={200="OK", "401"="Bad credentials"},
     *  tags={"tested" = "green"},
     *  parameters={
     *      {"name"="_username", "dataType"="string", "required"=true, "description"="Username or email"},
     *      {"name"="_password", "dataType"="string", "required"=true, "description"="User's password"}
     *  }
     * )
     */
    public function postSecurityLoginAction()
    {
        throw new \RuntimeException('You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.');
    }
}
