# Requirements
## Common Requirements
Those have to be present on all machines:

- VirtualBox 5.0.20 (64 bit)
- Vagrant 1.8.1
- Node 4.4 LTS

## Development Requirements
Those are only necessary for developers' machines:

- npm 3.8.9 or newer
- Python 2.7.11
- Ruby 2.0

> #### Windows
> Those are additional requirements for windows users:
> - Visual Studio Express

## Backend

> #####Windows
>In order to make nfs work on windows you have to install this plugin:
> ```sh
> vagrant plugin install vagrant-winnfsd
> ```

Up the vagrant for plugins:
```sh
vagrant up
```
and again for vm:
```sh
vagrant up
```

## Frontend
Go to webapp directory
```sh
cd webapp
```

Install NPM modules
```sh
npm install
```

Start Gulp server
```sh
npm run dev
```

Run unit tests
```sh
npm run test
```

Run unit tests in watch mode
```sh
npm run test:watch
```

## PHPUnit
```sh
vagrant ssh
cd /vagrant/backend/ && php bin/phpunit
```

With coverage <http://sandbox.dev/coverage/index.html>
```sh
cd /vagrant/backend/ && php bin/phpunit --coverage-html=/vagrant/backend/web/coverage
```

## Database
This command drops the database, adds a new one, creates entities and inserts fixtures
```sh
vagrant ssh
cd /vagrant/backend/ && php bin/console app:fixture:clear-database
```

## API Doc
<http://sandbox.dev/app_dev.php/api/doc>